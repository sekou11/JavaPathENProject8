TourGuide


About TourGuide

The TourGuide application allows via an internet connection to have information on tourist attractions, to propose the closest attractions according to user preferences. But also to accumulate points by visiting attractions in order to reuse them with our partner travel agencies.

 
APPLICATION
Project goals:
This application use external libraries
gpsUtil
rewardCentral
tripPricer
And tourGuide is the main application

 
 
Technical
1- Java 11
2- Gradle 7.4.2
 
Installing
A step by step series of examples that tell you how to get a development env running:
1.Install Java:
https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html
2.Install Gradle:
https://gradle.org/install/
 
Post installation of Java and Gradle, you have to :
fork this repository
 (https://github.com/OpenClassrooms-Student-Center/JavaPathENProject8)
Clone the project in your terminal : 
git clone https://github.com/OpenClassrooms-Student-Center/JavaPathENProject8.git
Running App
After some modifications of project we can run this app with gradle in command line:
gradle build : for build a classes in  folder build
mvn spring-boot:run : for run our application
